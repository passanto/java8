import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Java8Tester_4 {

    public static void main(String args[]) {
        Vehicle vehicle = new Car();
        vehicle.print();
        Random random = new Random(5);
        random.ints().limit(10).
                sorted().forEach(System.out::println);

        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);
        //get list of unique squares
        List<Integer> squaresList = numbers.stream().
                map(i -> i * i).
                distinct().
                collect(Collectors.toList());
        squaresList.forEach(n->System.out.print(n+", "));

        List<String>strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");

        //get count of empty string
        List<String> count = strings.
                stream().
                filter(string -> string.isEmpty() ||
                        !string.isEmpty() && string.startsWith("a")).
                collect(Collectors.toList());
        count.forEach(n->System.out.print(n+", "));

        System.out.println();
        System.out.println("Parallel");
        strings.parallelStream().
                filter(string -> !string.isEmpty()).
                forEach(n->System.out.print(n+", "));

        System.out.println();
        System.out.println("Parallel again");
        strings.parallelStream().
                filter(string -> !string.isEmpty()).
                forEach(n->System.out.print(n+", "));


        strings = Arrays.asList(
                "abc", "", "bc", "efg", "abcd","", "jkl");
        List<String> filtered = strings.stream().
                filter(string -> !string.isEmpty()).
                collect(Collectors.toList());

        System.out.println("Filtered List: " + filtered);
        String mergedString = strings.stream().
                filter(string -> !string.isEmpty()).
                collect(Collectors.joining(", "));
        System.out.println("Merged String: " + mergedString);

    }
}

interface Vehicle {

    default void print() {
        System.out.println("I am a vehicle!");
    }

    static void blowHorn() {
        System.out.println("Blowing horn!!!");
    }
}

interface FourWheeler {

    default void print() {
        System.out.println("I am a four wheeler!");
    }
}

class Car implements Vehicle, FourWheeler {

    public void print() {
        Vehicle.super.print();
        FourWheeler.super.print();
        Vehicle.blowHorn();
        System.out.println("I am a car!");
    }
}
