interface Executable {
	// functional interface
	// has just one method
	void execute();
}

class Runner {
	public void run(Executable e) {
		System.out.println("Executing...");
		e.execute();
	}
}

public class App {

	public static void main(String[] args) {
		Runner runner = new Runner();
		runner.run(new Executable() {
			public void execute() {
				System.out.println("Executed!");
			}
		});

		// lambda
		System.out.println("Enter Lambda..");
		runner.run(() -> {
			System.out.println("I'm super lambda");
			System.out.println("I'm super lambda again");
		});
	}

}
