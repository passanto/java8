import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Collectors.*;

public class StreamsAdvanced {


    public static void main(String[] args) {
        StreamsAdvanced sa = new StreamsAdvanced();
        List<PersonAdv> persons =
                Arrays.asList(
                        new PersonAdv("Max", 18),
                        new PersonAdv("Peter", 23),
                        new PersonAdv("Pamela", 23),
                        new PersonAdv("David", 12));
        List<PersonAdv> filtered =
                persons
                        .stream()
                        .filter(p -> p.name.startsWith("P"))
                        .collect(Collectors.toList());
        System.out.println(filtered);

        //groupingBy
        Map<Integer, List<PersonAdv>> personsByAge = persons
                .stream()
                .collect(Collectors.groupingBy(p -> p.age));
        personsByAge
                .forEach((age, p) -> System.out.format("Age %s: %s \n", age, p));

        //averagingInt
        Double averageAge = persons
                .stream()
                .collect(Collectors.averagingInt(p -> p.age));
        System.out.println(averageAge);     // 19.0

        //joining
        String phrase = persons
                .stream()
                .filter(p -> p.age >= 18)
                .map(p -> p.name)
                .collect(Collectors.joining(", ", "In Italy: ", " are of legal age."));
        System.out.println(phrase);

        //merge function
        Map<Integer, String> map = persons
                .stream()
                .collect(Collectors.toMap(
                        p -> p.age,
                        p -> p.name,
                        (name1, name2) -> name1 + ";" + name2));
        System.out.println(map);

        // customCollector
        Collector<PersonAdv, StringJoiner, String> personNameCollector =
                Collector.of(
                        () -> new StringJoiner(" | "),          // supplier
                        (j, p) -> j.add(p.name),  // accumulator
                        (j1, j2) -> j1.merge(j2),               // combiner
                        StringJoiner::toString);                // finisher

        String names = persons
                .stream()
                .collect(personNameCollector);
        System.out.println(names);  // MAX | PETER | PAMELA | DAVID



    }

}

class PersonAdv {
    String name;
    int age;

    PersonAdv(String name, int age) {
        this.name = name;
        this.age = age;


    }

    @Override
    public String toString() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
