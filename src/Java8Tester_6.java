import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Optional;

public class Java8Tester_6 {

   public static void main(String args[]) {
      Java8Tester_6 java8Tester = new Java8Tester_6();
      Integer value1 = null;
      Integer value2 = new Integer(10);
		
      //Optional.ofNullable - allows passed parameter to be null.
      Optional<Integer> a = Optional.ofNullable(value1);
		
      //Optional.of - throws NullPointerException if passed parameter is null
      Optional<Integer> b = Optional.of(value2);
      System.out.println(java8Tester.sum(a,b));


      ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
      ScriptEngine se = scriptEngineManager.getEngineByName("nashorn");

      String name = "Mahesh";
      Integer result = null;

      try {
         se.eval("print('" + name + "')");
         result = (Integer) se.eval("10 + 2");

      } catch(ScriptException e) {
         System.out.println("Error executing script: "+ e.getMessage());
      }
      System.out.println(result.toString());
   }
	
   public Integer sum(Optional<Integer> a, Optional<Integer> b) {
      //Optional.isPresent - checks the value is present or not
		
      System.out.println("First parameter is "+
              (!a.isPresent()?"not ":"")+ "present: " );
      System.out.println("Second parameter is present: " + b.isPresent());
		
      //Optional.orElse - returns the value if present otherwise returns
      //the default value passed.
      Integer value1 = a.orElse(new Integer(0));
		
      //Optional.get - gets the value, value should be present
      Integer value2 = b.get();
      return value1 + value2;
   }
}
