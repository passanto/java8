interface ExecutableInt {
	// functional interface
	// has just one method
	// this method gets implemented by the lambda
	int execute(int a, int b);
}

interface ExecutableString {
	// functional interface
	// has just one method
	// this method gets implemented by the lambda
	int execute(String a);
}

class RunnerInt {
	public void run(ExecutableInt e) {
		System.out.println("Executing...");
		int i = e.execute(2, 3);
		System.out.println("Value: " + i);
	}

	public void run(ExecutableString es) {
		System.out.println("Executing...");
		int i = es.execute("2");
		System.out.println("Value: " + i);
	}
}

public class App2 {

	public static void main(String[] args) {
		RunnerInt runner = new RunnerInt();
		runner.run(new ExecutableInt() {
			public int execute(int a, int b) {
				System.out.println("Executed!");
				return a + b;
			}
		});

		// lambda
		System.out.println("Enter Lambda..");
		runner.run((a, b) -> {
			System.out.println("From Lambda..");
			return a + b;
		});

		ExecutableInt ex = (a, b) -> {
			System.out.println("From Lambda..");
			return a + b;
		};
		runner.run(ex);
	}

}
