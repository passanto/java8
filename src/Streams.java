import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams {

	static List<Integer> values = Arrays.asList(1, 2, 3, 4, 5, 6);

	public static void main(String[] args) {
		System.out.println(values.stream().//
				map(e -> e * 2).//
				reduce(0, (c, e) -> c + e));
		System.out.println();

		List<String> myList = Arrays.asList("a1", "a2", "b1", "c2", "c1");
		myList.stream().//
				filter(s -> s.startsWith("c")).//
				map(String::toUpperCase).sorted().//
				forEach(System.out::println);
		System.out.println();

		Stream.of("a1", "a2", "a3").//
				findFirst().ifPresent(System.out::println); // a1
		System.out.println();

		IntStream.range(0, 4).forEach(System.out::println);
		System.out.println();

		IntStream.range(1, 4) //
				.map(n -> 2 * n + 1) //
				.average() //
				.ifPresent(System.out::println); // 5.0
		System.out.println();

		Stream.of("a1", "a2", "a3")//
				.map(s -> s.substring(1))//
				.mapToInt(Integer::parseInt)//
				.max()//
				.ifPresent(System.out::println); // 3
		System.out.println();

		IntStream.range(1, 4) //
				.mapToObj(i -> "a" + i) //
				.forEach(System.out::println);
		System.out.println();

		Stream.of(1.0, 2.0, 3.0) //
				.mapToInt(Double::intValue) //
				.mapToObj(i -> "a" + i) //
				.forEach(System.out::println);
		System.out.println("-");

		Stream.of("d2", "a2", "b1", "b3", "c").filter(s -> {
			System.out.println("filter: " + s);
			return true;
		}).forEach(s -> System.out.println("	forEach: " + s));
		System.out.println("-");

		Stream.of("d2", "a2", "b1", "b3", "c").map(s -> {
			System.out.println("map: " + s);
			return s.toUpperCase();
		}).anyMatch(s -> {
			System.out.println("anyMatch: " + s);
			return s.startsWith("A");
		});
		System.out.println("-");

		Stream.of("d2", "a2", "b1", "b3", "c").filter(s -> {
			System.out.println("filter: " + s);
			return s.startsWith("a");
		}).sorted((s1, s2) -> {
			System.out.printf("sort: %s; %s\n", s1, s2);
			return s1.compareTo(s2);
		}).map(s -> {
			System.out.println("map: " + s);
			return s.toUpperCase();
		}).forEach(s -> System.out.println("forEach: " + s));
		System.out.println("----");

		List<Person> persons = Arrays.asList(//
				new Person("Max", 18), //
				new Person("Peter", 23), //
				new Person("Pamela", 23), //
				new Person("David", 12));
		Map<Integer, List<Person>> m = //
				persons.stream().collect(//
						Collectors.groupingBy(p -> p.age));
		m.forEach((a, p) -> System.out.format("Age %s: %s\n", a, p));
		Double d = persons.stream().collect(//
				Collectors.averagingInt(p -> p.age));
		System.out.println(d);
	}

}

class Person {
	String name;
	int age;

	Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return name;
	}
}
